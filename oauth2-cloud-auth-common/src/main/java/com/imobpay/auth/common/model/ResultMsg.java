package com.imobpay.auth.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class ResultMsg<T> implements Serializable {
    private static final long serialVersionUID = 3422712299182301023L;
    private Integer code;

    private String msg;

    private T data;

    public static <T> ResultMsg<T> resultSuccess(T data) {
        return new ResultMsg<T>(200, "操作成功", data);
    }

    public static <T> ResultMsg<T> result(Integer code, String msg, T data) {
        return new ResultMsg<T>(code, msg, data);
    }
}
