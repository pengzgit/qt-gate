package com.imobpay.auth.server.controller;

import com.imobpay.auth.common.model.LoginVal;
import com.imobpay.auth.common.model.ResultMsg;
import com.imobpay.auth.common.model.SysConstant;
import com.imobpay.auth.common.utils.OauthUtils;
import com.imobpay.auth.server.service.impl.LoadRolePermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.CheckTokenEndpoint;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @other 这里不能添加其他地址，注意在com.imobpay.auth.server.config.SecurityConfig 里配置permitAll
 */
@RestController
@RequestMapping("/oauth")
@Slf4j
public class AuthController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private LoadRolePermissionService loadRolePermissionService;

    /**
     * 令牌请求的端点
     */
    @Autowired
    private TokenEndpoint tokenEndpoint;

    @Autowired
    private CheckTokenEndpoint checkTokenEndpoint;


    /**
     * 登出
     * @return
     */
    @PostMapping("/logout")
    public ResultMsg logout(){
        LoginVal loginVal = OauthUtils.getCurrentUser();
        log.info("令牌唯一ID：{},过期时间：{}",loginVal.getJti(),loginVal.getExpireIn());
        //这个jti放入redis中，并且过期时间设置为token的过期时间
        stringRedisTemplate.opsForValue().set(SysConstant.JTI_KEY_PREFIX+loginVal.getJti(),"",loginVal.getExpireIn(), TimeUnit.SECONDS);
        return new ResultMsg(200,"注销成功",null);
    }

    /**
     * 刷新权限
     * @ignore
     * @return
     */
    @RequestMapping("/refreshAuthorization")
    public ResultMsg refreshAuthorization() {
        loadRolePermissionService.init();
        return new ResultMsg(200,"刷新成功",null);
    }

    /**
     * @other 重写/oauth/token这个默认接口，返回的数据格式统一
     */
    @PostMapping(value = "/token")
    public ResultMsg<OAuth2AccessToken> postAccessToken(Principal principal, @RequestParam
            Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        OAuth2AccessToken accessToken = tokenEndpoint.postAccessToken(principal, parameters).getBody();
        return ResultMsg.resultSuccess(accessToken);
    }

    /**
     * @other 重写/oauth/check_token这个默认接口，用于校验令牌，返回的数据格式统一
     */
    @PostMapping(value = "/check_token")
    public ResultMsg<Map<String,?>> checkToken(@RequestParam("token") String value)  {
        Map<String, ?> map = checkTokenEndpoint.checkToken(value);
        return ResultMsg.resultSuccess(map);
    }

    @GetMapping("/error")
    @ResponseBody
    public String error(Model model, HttpServletResponse response){
        return "oauth-error";
    }
}
