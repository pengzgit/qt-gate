package com.imobpay.auth.server.controller;

import com.imobpay.auth.common.model.ResultMsg;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author margo
 * @date 2022/7/6
 */
@Controller
@SessionAttributes("authorizationRequest")
public class ApprovalController {

    // @RequestMapping("/oauth/confirm_access")
    // public ModelAndView getAccessConfirmation(Map<String, Object> model, HttpServletRequest request) throws Exception {
    //     AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.get("authorizationRequest");
    //     ModelAndView view = new ModelAndView();
    //     view.setViewName("base-grant");  // 自定义页面名字，resources\templates\base-grant.html
    //     view.addObject("clientId", authorizationRequest.getClientId());
    //     view.addObject("scopes", authorizationRequest.getScope());
    //     return view;
    // }

    @RequestMapping("/oauth/confirm_access")
    @ResponseBody
    public ResultMsg<Map<String, Object>> getAccessConfirmation(Map<String, Object> model, HttpServletRequest request) throws Exception {
        AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.get("authorizationRequest");
        Map<String, Object> result = new HashMap<>(2);
        result.put("clientId", authorizationRequest.getClientId());
        result.put("scopes", authorizationRequest.getScope());
        return ResultMsg.resultSuccess(result);
    }
}
