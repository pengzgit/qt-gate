package com.imobpay.auth.server.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.imobpay.auth.common.model.ResultCode;
import com.imobpay.auth.common.model.ResultMsg;
import com.imobpay.auth.common.model.SecurityUser;
import com.imobpay.auth.common.security.filter.AuthenticationFilter;
import com.imobpay.auth.server.sms.SmsCodeSecurityConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * spring security的安全配置
 */
@Configuration
@EnableWebSecurity
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private SmsCodeSecurityConfig smsCodeSecurityConfig;

    @Autowired
    private AuthenticationFilter myAuthenticationFilter;

    // @Autowired
    // private TokenAuthenticationProcessingFilter tokenAuthenticationProcessingFilter;


    // @Autowired
    // private TokenAuthenticationProvider tokenAuthenticationProvider;
    /**
     * 加密算法
     */
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //todo 允许表单登录
        http
                // .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                // .and()
                //注入自定义的授权配置类
                .apply(smsCodeSecurityConfig)
                .and()
                .authorizeRequests()
                //注销的接口需要放行
                .antMatchers("/oauth/logout", "/error").permitAll()
                // .antMatchers("/oauth/refreshAuthorization").hasAnyRole("ROOT", "ADMIN")
                .antMatchers("/oauth/refreshAuthorization").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/oauth/login")
                .successHandler((req, res, authentication) -> {
                            SecurityUser principal = (SecurityUser) authentication.getPrincipal();
                            principal.setPassword(null);
                            res.setContentType("application/json;charset=utf-8");
                            PrintWriter out = res.getWriter();
                            out.write(new ObjectMapper().writeValueAsString(ResultMsg.resultSuccess(principal)));
                            out.flush();
                            out.close();
                        })
                .failureHandler((req, res, e) -> {
                            res.setContentType("application/json;charset=utf-8");
                            log.info("req url: {} happened error {}", req.getRequestURI(), e.getMessage());
                            PrintWriter out = res.getWriter();
                            out.write(new ObjectMapper().writeValueAsString(ResultMsg.result(ResultCode.UNAUTHORIZED.getCode(), e.getMessage(), null)));
                            out.flush();
                            out.close();
                        })
                .permitAll()
                .and()
                // .disable()
                .csrf()
                .disable()
                .addFilterBefore(myAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                // .addFilterAfter(tokenAuthenticationProcessingFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint((req, res, authException) -> {
                            res.setContentType("application/json;charset=utf-8");
                            log.info("req url: {} happened error {}", req.getRequestURI(), authException.getMessage());
                            PrintWriter out = res.getWriter();
                            out.write(new ObjectMapper().writeValueAsString(ResultMsg.result(ResultCode.UNAUTHORIZED.getCode(), authException.getMessage(), null)));
                            out.flush();
                            out.close();
                        }
                )
                ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                // .authenticationProvider(tokenAuthenticationProvider)
                //从数据库中查询用户信息
                .userDetailsService(userDetailsService);
    }



    /**
     * AuthenticationManager对象在OAuth2认证服务中要使用，提前放入IOC容器中
     * Oauth的密码模式需要
     */
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}