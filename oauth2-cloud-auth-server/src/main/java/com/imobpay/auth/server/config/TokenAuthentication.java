package com.imobpay.auth.server.config;

import com.imobpay.auth.common.model.LoginVal;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * @author margo
 * @date 2022/6/20
 */
@Deprecated
public class TokenAuthentication extends AbstractAuthenticationToken {

    private Object credentials;
    private Object principal;

    /**
     * Creates a token with the supplied array of authorities.
     *
     * @param authorities the collection of <tt>GrantedAuthority</tt>s for the principal
     *                    represented by this authentication object.
     */
    public TokenAuthentication(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);

    }

    @Override
    public Object getCredentials() {
        return this.credentials;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

    public void setCredentials(Object credentials) {
        this.credentials = credentials;
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    @Override
    public Object getDetails() {
        return super.getDetails();
    }

    @Override
    public void setDetails(Object details) {
        super.setDetails(details);
    }

    public static TokenAuthentication convertFromLoginVal(LoginVal loginVal) {

        if(Objects.nonNull(loginVal)) {
            String[] authorities = loginVal.getAuthorities();
            Collection<SimpleGrantedAuthority> authoritiesCollection = Collections.emptyList();
            if(Objects.nonNull(authorities)) {
                authoritiesCollection = new ArrayList<>(authorities.length);
                for (String authority : authorities) {
                    SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(authority);
                    authoritiesCollection.add(grantedAuthority);
                }
            }

            TokenAuthentication tokenAuthentication = new TokenAuthentication(authoritiesCollection);
            tokenAuthentication.setPrincipal(loginVal.getUsername());
            tokenAuthentication.setDetails(loginVal);
            tokenAuthentication.setAuthenticated(true);
            return tokenAuthentication;
        }
        return null;
    }
}
