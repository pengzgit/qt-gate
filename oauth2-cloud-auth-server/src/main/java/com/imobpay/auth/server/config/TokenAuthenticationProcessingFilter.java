package com.imobpay.auth.server.config;

import com.imobpay.auth.common.model.LoginVal;
import com.imobpay.auth.common.model.RequestConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * @author margo
 * @date 2022/6/20
 * 这里添加自定义filter无效，因为{@link com.imobpay.auth.server.config.SecurityConfig#configure}  配置了form表单登录
 */
@Slf4j
// @Component
@Deprecated
public class TokenAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {


    protected TokenAuthenticationProcessingFilter() {
        super("/oauth/refreshAuthorization");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        log.info("request url: {}", request.getServletPath());
        LoginVal loginVal = (LoginVal) request.getAttribute(RequestConstant.LOGIN_VAL_ATTRIBUTE);

        if(Objects.nonNull(loginVal)) {
            return super.getAuthenticationManager().authenticate(TokenAuthentication.convertFromLoginVal(loginVal));
        }
        throw new RuntimeException("请先认证");
    }

    @Autowired
    @Override
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, failed);
    }
}
