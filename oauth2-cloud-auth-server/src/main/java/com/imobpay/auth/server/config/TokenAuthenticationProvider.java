package com.imobpay.auth.server.config;

import com.imobpay.auth.common.model.LoginVal;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author margo
 * @date 2022/6/20
 */
@Deprecated
// @Component
public class TokenAuthenticationProvider implements AuthenticationProvider {


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (!supports(authentication.getClass())) {
            return null;
        }
        if(authentication instanceof TokenAuthentication) {
            TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
            Object details = tokenAuthentication.getDetails();
            if(Objects.isNull(details)) {
                return null;
            }
            if(details instanceof LoginVal) {
                LoginVal loginVal = (LoginVal) details;
                return TokenAuthentication.convertFromLoginVal(loginVal);
            }
        }
        return null;
    }


    @Override
    public final boolean supports(Class<?> authentication) {
        return (TokenAuthentication.class.isAssignableFrom(authentication));
    }
}
