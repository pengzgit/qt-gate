# qt-gate

#### 介绍
使用spring gateway作为dubbo服务网关的演示项目。该项目网关oauth2.0服务代码感谢 **公众号：码猿技术专栏** 微信公众号的陈老师。
| 功能点 | 是否实现 |   |
|---------------|---|---|
| oauth2.0认证、授权| √| |
| 整合dubbo服务端  | √ |   |
| Sentinel Dubbo token server集群限流 | √  |   |
| Sentinel Dubbo 单节点限流  | √  |   |
| 网关通过nacos动态控制外网url地址是否能访问 | √  |   |
| 网关添加nacos动态路由| √ | |
| 网关添加nacos动态URL白名单| √ | |
| 网关和认证服务添加smart doc接口文档| √ | |


#### 软件架构
软件架构说明
- oauth2-cloud-gateway：网关
- oauth2-cloud-auth-common：oauth2.0服务端相关依赖代码
- oauth2-cloud-auth-server：oauth2.0服务端
- oauth2-cloud-order-service：order微服务测试接口代码
- oauth2-cloud-product-service：product微服务测试接口代码
- dubbo-api：dubbo相关实体类
- dubbo-consumer1：dubbo消费端
- dubbo-provider1：dubbo服务端1
- dubbo-provider2：dubbo服务端2
- token-server：sentinel 的token server
![输入图片说明](images/image13.png)

#### 安装教程

1.  本地启动nacos
2.  本地启动sentinel
3.  启动oauth2-cloud-auth-server服务
4.  启动oauth2-cloud-gateway网关
5.  启动dubbo-consumer1
6.  启动dubbo-provider1，启动时添加jvm参数 -Djava.net.preferIPv4Stack=true -Dcsp.sentinel.dashboard.server=localhost:8858 -Dproject.name=dub-provider1 -Dcsp.sentinel.log.use.pid=true
7.  启动dubbo-provider2，启动时添加jvm参数 -Djava.net.preferIPv4Stack=true -Dcsp.sentinel.dashboard.server=localhost:8858 -Dproject.name=dub-provider2 -Dcsp.sentinel.log.use.pid=true
8.  启动token-server服务
9.  在sentinel的各服务端配置token server

#### 使用说明

![sentinel provider1 配置token server](images/image1.png)
![sentinel provider2 配置token server](images/image2.png)
![provider1 配置流控](images/image3.png)
![provider1 配置流控明细](images/image4.png)
![provider2 配置流控](images/image9.png)
![postman配置接口压测](images/image5.png)
![压测运行](images/image6.png)
![provider1 sentinel流控实时数据](images/image7.png)
![provider2 sentinel流控实时数据](images/image8.png)
![nacos动态网关配置文件](images/image26.png)
![nacos动态网关授权配置文件详情](images/image11.png)
![nacos动态URL白名单](images/image23.png)
![网关基于nacos的动态路由](images/image24.png)
![网关和oauth2服务启动](images/image12.png)
![postman 未登录时尝试使用获取授权码](images/image13.png)
![postman 通过网关使用用户名和密码登录](images/image15.png)


/oauth/confirm_access 接口未改前，返回默认自带授权页面
![postman已登录状态下再次尝试获取授权码显示出授权页面](images/image16.png)


/oauth/confirm_access 接口改为返回JSON
![需要授权接口返回JSON](images/image17.png)

访问http://localhost:3001/grant 进入到自定义授权页面，进行授权
![自定义授权页面](images/image18.png)

点击同意授权
![获取到授权码](images/image19.png)

通过授权码获取token
![通过授权码获取token](images/image21.png)

点击拒绝授权
![输入图片说明](images/image20.png)


#### token server 分布式高可用思路
1. When token server starting, connect zookeeper and add node listener, register itself to zookeeper
2. Token client subscribe the directory of token server zk nodes
3. Zookeeper listen the change of token server nodes, when a server unavailable, notify the last updated available nodes to token client, then client cache the available nodes in local
4. After token client accept the flow request, get the available token server nodes from local cache ，and then load balance to select one node