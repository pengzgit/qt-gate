package com.imobpay.provider2.impl;

import com.imobpay.api.demo.SentinelService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

import java.time.LocalDateTime;

/**
 * @author margo
 * @date 2022/6/21
 */
@DubboService
@Slf4j
public class SentinelServiceImpl implements SentinelService {

    @Override
    public String sayHello(String txt) {
        log.info("provider2 {}, time: {}", txt, LocalDateTime.now());
        return String.format("this is provider2 name: %s, now: %s", txt, LocalDateTime.now().toString());
    }
}