package com.imobpay.provider2;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 */
@EnableDubbo
@SpringBootApplication
public class Provider2 {
    public static void main(String[] args) {
        SpringApplication.run(Provider2.class, args);
    }
}
