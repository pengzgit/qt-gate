package com.imobpay.tokenserver;

/**
 * @author margo
 * @date 2022/6/21
 */
public final class ServerConstants {

    public static final String APP_NAME = "appA";

    public static final String FLOW_POSTFIX = "-flow-rules";
    public static final String PARAM_FLOW_POSTFIX = "-param-rules";
}
