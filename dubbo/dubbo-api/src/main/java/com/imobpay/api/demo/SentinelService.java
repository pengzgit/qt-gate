package com.imobpay.api.demo;

/**
 * 测试接口
 *
 * @author margo 2022/7/1.
 * @version 1.0.0
 * @dubbo
 */
public interface SentinelService {

    /**
     * hello
     * @param txt
     * @return
     */
    String sayHello(String txt);
}
