package com.imobpay.consumer1.service;

/**
 * @author margo
 * @date 2022/6/21
 */
public interface ConsumerSentinelService {

    String consumer(String name);
}
