package com.imobpay.consumer1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 * https://github.com/alibaba/Sentinel/wiki/%E4%BB%8B%E7%BB%8D
 *
 */
@SpringBootApplication
public class Consumer1 {
    public static void main(String[] args) {
        SpringApplication.run(Consumer1.class, args);
    }
}
