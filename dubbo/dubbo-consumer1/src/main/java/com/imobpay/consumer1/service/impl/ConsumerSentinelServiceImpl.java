package com.imobpay.consumer1.service.impl;

import com.imobpay.api.demo.SentinelService;
import com.imobpay.consumer1.service.ConsumerSentinelService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

/**
 * @author margo
 * @date 2022/6/21
 */
@Service
public class ConsumerSentinelServiceImpl implements ConsumerSentinelService {

    @DubboReference
    private SentinelService sentinelService;

    @Override
    public String consumer(String name) {
        return sentinelService.sayHello(name);
    }
}
