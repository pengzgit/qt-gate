package com.imobpay.consumer1.controller;

import com.imobpay.consumer1.service.ConsumerSentinelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author margo
 * @date 2022/6/21
 */
@RestController
@Slf4j
public class Consumer1Controller {

    @Autowired
    private ConsumerSentinelService consumerSentinelService;

    @GetMapping("testConsumer1")
    public String testConsumer1(String name){
        log.info("testConsumer1 name: {}", name);
        return consumerSentinelService.consumer(name);
    }
}
