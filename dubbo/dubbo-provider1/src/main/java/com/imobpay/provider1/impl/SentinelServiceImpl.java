package com.imobpay.provider1.impl;

import com.imobpay.api.demo.SentinelService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

import java.time.LocalDateTime;

/**
 * @author margo
 * @date 2022/6/21
 */
@DubboService
@Slf4j
public class SentinelServiceImpl implements SentinelService {

    /**
     * hello实现1
     * @param txt 文本|法外狂徒
     * @return
     */
    @Override
    public String sayHello(String txt) {
        log.info("provider1 {}, time: {}", txt, LocalDateTime.now());
        return String.format("this is provider1 name: %s, now: %s", txt, LocalDateTime.now().toString());
    }
}