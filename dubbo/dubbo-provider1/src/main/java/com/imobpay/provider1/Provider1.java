package com.imobpay.provider1;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 */
@EnableDubbo
@SpringBootApplication
public class Provider1 {
    public static void main(String[] args) {
        SpringApplication.run(Provider1.class, args);
    }
}
