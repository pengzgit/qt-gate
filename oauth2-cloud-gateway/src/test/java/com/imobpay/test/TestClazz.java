package com.imobpay.test;

import cn.hutool.core.lang.Dict;
import cn.hutool.setting.yaml.YamlUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.deserializer.*;
import com.imobpay.gateway.model.DmzConfig;
import com.imobpay.gateway.model.DmzDict;
import org.junit.Test;

import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author margo
 * @date 2022/6/21
 */
public class TestClazz {

    @Test
    public void test1() {
        String s = "{\"urls\":[{\"aaa\":true},{\"bbb\":false}],\"enable\":true}";
        DmzConfig dmzConfig = JSON.parseObject(s, DmzConfig.class, new CustomParseProcess() );
        System.out.println("dmzConfig = " + dmzConfig);
    }

    static class CustomParseProcess implements ExtraProcessor, ExtraTypeProvider{


        @Override
        public void processExtra(Object object, String key, Object value) {
            DmzDict dmzDict = (DmzDict) object;
            dmzDict.setUrlPath(key);
            dmzDict.setAccessible(Boolean.parseBoolean(String.valueOf(value)));
        }

        @Override
        public Type getExtraType(Object object, String key) {
            return String.class;
        }
    }

    @Test
    public void test() throws Exception {
        //单一线程池
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        //InheritableThreadLocal存储
        InheritableThreadLocal<String> username = new InheritableThreadLocal<>();
        for (int i = 0; i < 10; i++) {
            username.set("计数—" + i);
            Thread.sleep(3000);
            CompletableFuture.runAsync(() -> System.out.println(username.get()), executorService);
        }
    }

    @Test
    public void test2() {
        String ignoreUrlPath = "oauth2.cloud.sys.parameter.ignoreUrls";

        String s = "oauth2:\n" +
                "  cloud:\n" +
                "    sys:\n" +
                "      parameter:\n" +
                "        ignoreUrls:\n" +
                "          - /oauth/token\n" +
                "          - /oauth/authorize\n" +
                "          - /oauth/login\n" +
                "          - /oauth/logout\n" +
                "          - /gate/index\n" +
                "          - /login\n" +
                "          - /index\n" +
                "          - /grant\n" +
                "          - /vendors/**\n" +
                "          - /css/**\n" +
                "          - /js/**\n" +
                "          - /favicon.ico";
        Map<String, Object> load = YamlUtil.load(new StringReader(s), Map.class);
        String[] split = ignoreUrlPath.split("\\.");
        Map<String, Object> resultTemp = load;
        List<String> list = null;
        for (String level : split) {
            if("ignoreUrls".equals(level)) {
                list = (List<String>) resultTemp.get(level);
                break;
            }
            resultTemp = (Map<String, Object>) resultTemp.get(level);
        }
        System.out.println("list = " + list);
    }

}
