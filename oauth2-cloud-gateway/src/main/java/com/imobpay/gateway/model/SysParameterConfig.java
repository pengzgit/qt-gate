package com.imobpay.gateway.model;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.List;

/**
 */
@ConfigurationProperties(prefix = "oauth2.cloud.sys.parameter")
@Data
@RefreshScope
public class SysParameterConfig {
    /**
     * 白名单
     */
    private List<String> ignoreUrls;
}
