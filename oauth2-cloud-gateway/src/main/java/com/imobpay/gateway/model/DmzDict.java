package com.imobpay.gateway.model;

import lombok.ToString;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Map;

/**
 * @author margo
 * @date 2022/6/21
 */
@ToString
public class DmzDict implements Map.Entry<String, Boolean>, Serializable{
    private static final long serialVersionUID = 2050955508824614042L;
    private String urlPath;
    /**
     * true 允许外网访问
     * false 外网禁止访问
     */
    private Boolean accessible;

    public DmzDict() {}

    public DmzDict(String urlPath, Boolean accessible) {
        this.urlPath = urlPath;
        this.accessible = accessible;
    }

    @Override
    public String getKey() {
        return urlPath;
    }

    @Override
    public Boolean getValue() {
        return accessible;
    }

    @Override
    public Boolean setValue(Boolean value) {
        Boolean old = this.accessible;
        this.accessible = value;
        return old;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public void setAccessible(Boolean accessible) {
        this.accessible = accessible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DmzDict dmzDict = (DmzDict) o;

        return new EqualsBuilder()
                .append(urlPath, dmzDict.urlPath)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(urlPath)
                .toHashCode();
    }
}
