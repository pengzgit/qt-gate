package com.imobpay.gateway.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author margo
 * @date 2022/6/21
 */
@RestController
@RequestMapping(("/gate"))
@Slf4j
@RefreshScope
public class HelloController {

    @Value("${name}")
    private String name;

    /**
     * 欢迎页
     * @return hello gateway
     */
    @RequestMapping("/index")
    public String index() {
        log.info("name: {}", name);
        return "hello gateway";
    }
}
