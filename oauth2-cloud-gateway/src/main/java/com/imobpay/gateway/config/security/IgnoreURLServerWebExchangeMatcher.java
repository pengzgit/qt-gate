package com.imobpay.gateway.config.security;

import com.imobpay.gateway.model.SysParameterConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.pattern.PathPatternParser;
import reactor.core.publisher.Mono;

import java.util.Objects;

/**
 * @author margo
 * @date 2022/7/8
 */
@Component
@Slf4j
public class IgnoreURLServerWebExchangeMatcher implements ServerWebExchangeMatcher {

    private static final PathPatternParser DEFAULT_PATTERN_PARSER = new PathPatternParser();

    @Autowired
    private SysParameterConfig sysConfig;

    @Override
    public Mono<MatchResult> matches(ServerWebExchange exchange) {
        ServerHttpRequest request = exchange.getRequest();
        log.info("ignore url matcher: {}", request.getPath());
        if(Objects.isNull(sysConfig) || Objects.isNull(sysConfig.getIgnoreUrls()) || sysConfig.getIgnoreUrls().isEmpty()) {
            // 没有配置直接跳过
            return MatchResult.match();
        }
        ServerWebExchangeMatcher pathMatchers = ServerWebExchangeMatchers.pathMatchers(sysConfig.getIgnoreUrls().toArray(new String[0]));
        return pathMatchers.matches(exchange);
    }
}
