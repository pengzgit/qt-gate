package com.imobpay.gateway.config;

import cn.hutool.setting.yaml.YamlUtil;
import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.config.listener.AbstractListener;
import com.imobpay.gateway.model.DmzConfig;
import com.imobpay.gateway.model.DmzDict;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author margo
 * @date 2022/6/21
 */
@Component
@Slf4j
public class NacosConfigDMZConfiguration extends AbstractListener {

    private static final String DATA_ID = "gateway-dmz.yaml";
    private static final String GROUP = "DEFAULT_GROUP";

    @Autowired
    private NacosConfigManager nacosConfigManager;

    @Autowired
    private DmzConfig dmzConfig;

    @Bean
    public ApplicationRunner runnerDmz() {
        return args -> {
            String configInfo = nacosConfigManager.getConfigService().getConfigAndSignListener(DATA_ID, GROUP, 1000, this);
            if (!StringUtils.isBlank(configInfo)) {
                receiveConfigInfo(configInfo);
            }
        };
    }


    @Override
    public void receiveConfigInfo(String configInfo) {
        try {
            log.info("gateway-dmz configInfo:\n {}", configInfo);
            Map<String, Map<String, Object>> load = YamlUtil.load(new StringReader(configInfo), Map.class, true);
            log.info("load: {}", load);
            if(Objects.isNull(load) || load.isEmpty() || !load.containsKey(DmzConfig.DMZ_KEY) || Objects.isNull(load.get(DmzConfig.DMZ_KEY))) {
                // 认为配置有误 不改变系统内配置
                return;
            }
            Map<String, Object> mapConfig = load.get(DmzConfig.DMZ_KEY);
            DmzConfig dmzConfig = JSON.parseObject(JSON.toJSONString(mapConfig), DmzConfig.class, new DmzConfig.CustomDmzConfigParseProcess());
            Set<DmzDict> urls = dmzConfig.getUrls();
            boolean enable = dmzConfig.isEnable();
            if(!enable || Objects.isNull(urls) || urls.isEmpty()) {
                updateDmzConfig(enable, new HashSet<>(0));
                return;
            }
            updateDmzConfig(enable, urls);
        } catch (Exception e) {
            log.info("变更nacos动态配置dmz网址可见失败 {}", e.getMessage());
            log.error("变更nacos动态配置dmz网址可见失败 {}", e.getMessage(), e);
        }
    }

    private void updateDmzConfig(boolean enable,  Set<DmzDict> data) {
        dmzConfig.setEnable(enable);
        Set<DmzDict> temp = new HashSet<>(data.size());
        DmzDict tempDmzDict = null;
        for (DmzDict datum : data) {
            tempDmzDict = new DmzDict(datum.getKey(), datum.getValue());
            temp.add(tempDmzDict);
        }
        dmzConfig.setUrls(temp);
    }

    @PostConstruct
    public void init() {

    }

    @PreDestroy
    public void destroy() {

    }
}
