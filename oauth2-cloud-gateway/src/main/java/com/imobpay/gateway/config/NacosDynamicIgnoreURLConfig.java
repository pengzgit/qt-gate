package com.imobpay.gateway.config;

import cn.hutool.core.lang.Dict;
import cn.hutool.setting.yaml.YamlUtil;
import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.nacos.api.config.listener.AbstractListener;
import com.imobpay.gateway.model.DmzConfig;
import com.imobpay.gateway.model.SysParameterConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.json.YamlJsonParser;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.StringReader;
import java.util.*;

/**
 * @author margo
 * @date 2022/7/8
 */
@Configuration
@Slf4j
public class NacosDynamicIgnoreURLConfig extends AbstractListener implements ApplicationEventPublisherAware {

    private static final String DATA_ID = "gateway-ignore-url.yaml";
    private static final String GROUP = "DEFAULT_GROUP";
    private static final String ignoreUrlPath = "oauth2.cloud.sys.parameter.ignoreUrls";
    private static final String FINAL_PATH = "ignoreUrls";

    @Autowired
    private NacosConfigManager nacosConfigManager;

    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private SysParameterConfig sysConfig;

    @Override
    public void receiveConfigInfo(String configInfo) {
        log.info("gateway ignore url: \n {}", configInfo);
        try {
            Map<String, Object> load = YamlUtil.load(new StringReader(configInfo), Map.class);
            String[] split = ignoreUrlPath.split("\\.");
            List<String> urls = null;
            for (String level : split) {
                if(FINAL_PATH.equals(level)) {
                    urls = (List<String>) load.get(level);
                    break;
                }
                if(!load.containsKey(level) || Objects.isNull(load.get(level))) {
                    break;
                }
                load = (Map<String, Object>) load.get(level);
            }
            if(Objects.isNull(urls)) {
                // 没有读取到，不处理
                return;
            }
            List<String> urlsTemp = new ArrayList<>(urls.size());
            urlsTemp.addAll(urls);
            sysConfig.setIgnoreUrls(urlsTemp);
            log.info("update sysConfig ignore urls successfully: {}", sysConfig);
        } catch (Exception e) {
            log.info("动态处理URL白名单报错: {}", e.getMessage(), e);
        }
    }


    @Bean
    public ApplicationRunner runnerIgnoreURL() {
        return args -> {
            String configInfo = nacosConfigManager.getConfigService().getConfigAndSignListener(DATA_ID, GROUP, 1000, this);
            if (!StringUtils.isBlank(configInfo)) {
                receiveConfigInfo(configInfo);
            }
        };
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
