package com.imobpay.gateway.router;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

/**
 * @author margo
 * @date 2022/7/5
 */
@Component
public class GateRouter {

    @Bean
    public RouterFunction<ServerResponse> loginRouter(
            @Value("classpath:/static/login.html") final Resource indexHtml) {
        return route(GET("/login"), request -> ok().contentType(MediaType.TEXT_HTML).syncBody(indexHtml));
    }

    @Bean
    public RouterFunction<ServerResponse> indexRouter(
            @Value("classpath:/static/index.html") final Resource indexHtml) {
        return route(GET("/index"), request -> ok().contentType(MediaType.TEXT_HTML).syncBody(indexHtml));
    }

    @Bean
    public RouterFunction<ServerResponse> grantRouter(
            @Value("classpath:/static/base-grant.html") final Resource indexHtml) {
        return route(GET("/grant"), request -> ok().contentType(MediaType.TEXT_HTML).syncBody(indexHtml));
    }

}
